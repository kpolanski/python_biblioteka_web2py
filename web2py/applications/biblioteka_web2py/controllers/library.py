# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# This is a sample controller
# this file is released under public domain and you can use without limitations
# -------------------------------------------------------------------------
import hashlib


def home():
    response.view = 'library/login.html'
    if session.logged_in:
        return redirect(URL('list_books'))
    return dict(books=None)


def login():
    response.view = 'library/login.html'
    username = request.vars['username']
    password = hashlib.md5(request.vars['password'].encode('utf-8'))
    user = db((db.users.username == username) & (db.users.password == password.hexdigest())).select().first()
    if user:
        session.logged_in = True
    return redirect(URL('home'))


def logout():
    session.logged_in = False
    return redirect(URL('home'))


def list_books():
    books = SQLFORM.smartgrid(db.books, user_signature=False)
    return dict(books=books)
